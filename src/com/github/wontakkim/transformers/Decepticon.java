package com.github.wontakkim.transformers;

public class Decepticon extends Transformer {

    public Decepticon(String name, int strength, int intelligence, int speed, int endurance, int rank, int courage, int firepower, int skill) {
        super(name, strength, intelligence, speed, endurance, rank, courage, firepower, skill);
    }
}