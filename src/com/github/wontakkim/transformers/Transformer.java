package com.github.wontakkim.transformers;

public abstract class Transformer {

    private static final int MIN_POINT = 1;
    private static final int MAX_POINT = 10;

    private String name;
    private int strength;
    private int intelligence;
    private int speed;
    private int endurance;
    private int rank;
    private int courage;
    private int firepower;
    private int skill;

    public Transformer(String name, int strength, int intelligence, int speed, int endurance, int rank, int courage, int firepower, int skill) {
        this.name = name;
        this.strength = Math.max(MIN_POINT, Math.min(strength, MAX_POINT));
        this.intelligence = Math.max(MIN_POINT, Math.min(intelligence, MAX_POINT));
        this.speed = Math.max(MIN_POINT, Math.min(speed, MAX_POINT));
        this.endurance = Math.max(MIN_POINT, Math.min(endurance, MAX_POINT));
        this.rank = Math.max(MIN_POINT, Math.min(rank, MAX_POINT));
        this.courage = Math.max(MIN_POINT, Math.min(courage, MAX_POINT));
        this.firepower = Math.max(MIN_POINT, Math.min(firepower, MAX_POINT));
        this.skill = Math.max(MIN_POINT, Math.min(skill, MAX_POINT));
    }

    public String getName() {
        return name;
    }

    public int getStrength() {
        return strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getSpeed() {
        return speed;
    }

    public int getEndurance() {
        return endurance;
    }

    public int getRank() {
        return rank;
    }

    public int getCourage() {
        return courage;
    }

    public int getFirepower() {
        return firepower;
    }

    public int getSkill() {
        return skill;
    }

    public int getOverallRating() {
        return strength + intelligence + speed + endurance + firepower;
    }

    public boolean isSpecial() {
        return name.equals("Optimus Prime") || name.equals("Predaking");
    }

    public static class Comparator implements java.util.Comparator<Transformer> {

        @Override
        public int compare(Transformer t1, Transformer t2) {
            return (t1.rank > t2.rank) ? -1 : ((t1.rank == t2.rank) ? 0 : 1);
        }
    }
}
