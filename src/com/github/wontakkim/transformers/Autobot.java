package com.github.wontakkim.transformers;

public class Autobot extends Transformer {

    public Autobot(String name, int strength, int intelligence, int speed, int endurance, int rank, int courage, int firepower, int skill) {
        super(name, strength, intelligence, speed, endurance, rank, courage, firepower, skill);
    }
}
