package com.github.wontakkim;

import com.github.wontakkim.transformers.Autobot;
import com.github.wontakkim.transformers.Decepticon;
import com.github.wontakkim.transformers.Transformer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Battle {

    private List<Transformer> autobots = new ArrayList();
    private List<Transformer> decepticons = new ArrayList();

    public void addFighter(String name, char identifier, int strength, int intelligence, int speed, int endurance, int rank, int courage, int firepower, int skill) {
        if (name == null || name.isEmpty())
            return;

        switch (identifier) {
            case 'A':
                autobots.add(new Autobot(name, strength, intelligence, speed, endurance, rank, courage, firepower, skill));
                break;

            case 'D':
                decepticons.add(new Decepticon(name, strength, intelligence, speed, endurance, rank, courage, firepower, skill));
                break;
        }
    }

    public void clear() {
        autobots.clear();
        decepticons.clear();
    }

    public void run() {
        autobots.sort(new Transformer.Comparator());
        decepticons.sort(new Transformer.Comparator());

        Set<Transformer> autobotDestroyed = new HashSet();
        Set<Transformer> decepticonDestroyed = new HashSet();
        int round = Math.min(autobots.size(), decepticons.size());

        for (int i = 0; i < round; i++) try {
            Transformer autobot = autobots.get(i);
            Transformer decepticon = decepticons.get(i);

            int result = fight(autobot, decepticon);
            if (result > 0) { // decepticon was destroyed
                decepticonDestroyed.add(decepticon);
            } else if (result < 0) { // autobot was destroyed
                autobotDestroyed.add(autobot);
            } else { // decepticon and autobot both were destroyed
                autobotDestroyed.add(autobot);
                decepticonDestroyed.add(decepticon);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("All competitors destroyed.");
            return;
        }

        showResult(round, autobotDestroyed, decepticonDestroyed);
    }

    private int fight(Transformer t1, Transformer t2) {
        if (t1.isSpecial() && t2.isSpecial()) {
            throw new IllegalArgumentException();
        }

        if (t1.isSpecial()) {
            return 1;
        }

        if (t2.isSpecial()) {
            return -1;
        }

        int courageDiff = t1.getCourage() - t2.getCourage();
        int strengthDiff = t1.getStrength() - t2.getStrength();
        int skillDiff = t1.getSkill() - t2.getSkill();
        int overallRatingDiff = t1.getOverallRating() - t2.getOverallRating();

        if ((courageDiff >= 4 && strengthDiff >= 3) || skillDiff >= 3) {
            return 1;
        }

        if ((courageDiff <= -4 && strengthDiff <= -3) || skillDiff <= -3) {
            return -1;
        }

        return (overallRatingDiff > 0) ? 1 : ((overallRatingDiff == 0) ? 0 : -1);
    }

    private void showResult(int round, Set<Transformer> autobotDestroyed, Set<Transformer> decepticonDestroyed) {
        System.out.printf("%d battle\n", round);

        int autobotScore = decepticonDestroyed.size();
        int decepticonScore = autobotDestroyed.size();

        if (autobotScore > decepticonScore) {
            showWiningTeam("Autobots", buildSurvivorsString(autobots, autobotDestroyed));
            showLosingTeam("Decepticons", buildSurvivorsString(decepticons, decepticonDestroyed));
        } else if (autobotScore < decepticonScore) {
            showWiningTeam("Decepticons", buildSurvivorsString(decepticons, decepticonDestroyed));
            showLosingTeam("Autobots", buildSurvivorsString(autobots, autobotDestroyed));
        } else {
            System.out.println("Draw");
        }
    }

    private String buildSurvivorsString(List<Transformer> transformers, Set<Transformer> destroyed) {
        String str = "";
        for (Transformer transformer : transformers) {
            if (!destroyed.contains(transformer)) {
                if (!str.isEmpty()) {
                    str += ", ";
                }
                str += transformer.getName();
            }
        }
        return str;
    }

    private void showWiningTeam(String team, String survivorsStr) {
        System.out.printf("Winning team (%s): %s\n", team, survivorsStr);
    }

    private void showLosingTeam(String team, String survivorsStr) {
        System.out.printf("Survivors from the losing team (%s): %s\n", team, survivorsStr);
    }
}
