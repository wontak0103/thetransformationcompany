package com.github.wontakkim;

public class Main {

    public static void main(String[] args) {
        Battle battle = new Battle();

        battle.addFighter("Soundwave", 'D', 8, 9, 2, 6, 7, 5, 6, 10);
        battle.addFighter("Bluestreak", 'A', 6, 6, 7, 9, 5, 2, 9, 7);
        battle.addFighter("Hubcap", 'A', 4, 4, 4, 4, 4, 4, 4, 4);
        battle.run();
        battle.clear();
        System.out.println();

        battle.addFighter("A", 'A', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("B", 'D', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.run();
        battle.clear();
        System.out.println();

        battle.addFighter("A", 'A', 5, 1, 1, 1, 1, 5, 1, 1);
        battle.addFighter("B", 'D', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("A", 'A', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("B", 'D', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.run();
        battle.clear();
        System.out.println();

        battle.addFighter("Optimus Prime", 'A', 1, 1, 1, 1, 10, 1, 1, 1);
        battle.addFighter("A", 'A', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("B", 'A', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("C", 'D', 1, 1, 1, 1, 3, 1, 1, 1);
        battle.addFighter("D", 'D', 1, 10, 1, 1, 2, 1, 1, 1);
        battle.addFighter("Predaking", 'D', 1, 10, 1, 1, 1, 1, 1, 1);
        battle.run();
        battle.clear();
        System.out.println();

        battle.addFighter("Optimus Prime", 'A', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.addFighter("Predaking", 'D', 1, 1, 1, 1, 1, 1, 1, 1);
        battle.run();
        battle.clear();
    }
}
